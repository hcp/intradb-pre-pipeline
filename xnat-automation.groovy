import java.lang.ProcessBuilder
import java.net.InetAddress
import java.net.UnknownHostException
import org.nrg.xdat.XDAT
import org.nrg.xdat.entities.AliasToken
import org.nrg.xdat.services.AliasTokenService
import org.nrg.xdat.om.XnatExperimentdata


// Find the localhost where this will be running
def localhost = null
try {
    localhost = InetAddress.getLocalHost()
} catch (UnknownHostException unknownHostException) {
    println "ERROR: Unknown Host " + unknownHostException.getMessage()
    throw unknownHostException
}

// Set up our parameters for the external script
def experimentData = XnatExperimentdata.getXnatExperimentdatasById(dataId, user, false)
def sessionLabel = experimentData.getLabel()
def projectId = externalId.toString()
def token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user)

// Check for empy argument json (auto-run) and set defaults
def args = null
if (arguments == "{}") {
    args = [:]
    args.scantype_file = "lifespan-scan-map.json"
} else {
    def slurper = new groovy.json.JsonSlurper()
    args = slurper.parseText(arguments)
}

// Helper methods to execute our script
private def executeOnShell(String command) {
  println command
  def process = new ProcessBuilder(addShellPrefix(command)).redirectErrorStream(true).start()
  process.inputStream.eachLine { println it }
  process.waitFor();
  return process.exitValue()
}

private def addShellPrefix(String command) {
  commandArray = new String[3]
  commandArray[0] = "sh"
  commandArray[1] = "-c"
  commandArray[2] = command
  return commandArray
}

// Execute our script on the shell
def command = "source /nrgpackages/scripts/epd-python_setup.sh && " +
    "python /nrgpackages/tools.release/intradb/pre-pipeline/pre-pipeline.py" +
    " -H https://" + localhost.getCanonicalHostName() + " -P " + projectId +
    " -e " + sessionLabel + " -u " + token.getAlias() + " -p " + token.getSecret() +
    " -m " + "/data/intradb/archive/" + projectId + "/resources/scantype_map/" +
    args.scantype_file
// f.append('\n\n' + command)

try {
    executeOnShell(command)
} catch (e) {
    println "EXCEPTION:  " + e
}

println "Finished processing"
