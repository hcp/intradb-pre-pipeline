import sys
import json

sys.path.append("/nrgpackages/tools.release/intradb")
from IntradbPipeline.params import parser
from IntradbPipeline.common import Pipeline
from hcpxnat.interface import HcpInterface

# Add params specific to this pipeline here
# Other parameters are pulled from the generic params.py
parser.add_option("-m", "--scan-map", action="store", type="string",
                  dest="scan_map", default="none", help='Json file ' +
                  'defining rules for mapping series desc to scan type.')
(opts, args) = parser.parse_args()

xnat = HcpInterface(url=opts.hostname, username=opts.alias,
                    password=opts.secret, project=opts.project)
xnat.session_label = opts.session
xnat.subject_label = xnat.getSessionSubject()
pipe = Pipeline(xnat, opts, name='prepipe')


def main():
    fixSeriesDescriptions()
    setScanTypes()


def fixSeriesDescriptions():
    # Strip or replace offending characters, underscores and parentheses
    scans = xnat.getSessionScans()

    for s in scans:
        xnat.scan_id = s['ID']
        sd = s['series_description']
        ## TODO:  Not yet sure we want expanded list for all projects.  Should this be configurable?
        if xnat.project in [ 'CCF_ECP_PRC','CCF_ADCPWI_PRC' ]:
            new_sd = sd.replace(' ', '_') \
                       .replace('(', '_') \
                       .replace('+', '_') \
                       .replace('=', '_') \
                       .replace(':', '') \
                       .replace('.', '') \
                       .replace(')', '') \
                       .replace('<', '') \
                       .replace('>', '') \
                       .replace('[', '') \
                       .replace(']', '') \
                       .replace('&lt;', '') \
                       .replace('&gt;', '') \
                       .replace('_RR', '')
        else:
            new_sd = sd.replace(' ', '_') \
                       .replace('(', '_') \
                       .replace(')', '') \
                       .replace('<', '') \
                       .replace('>', '') \
                       .replace('[', '') \
                       .replace(']', '') \
                       .replace('&lt;', '') \
                       .replace('&gt;', '') \
                       .replace('_RR', '')
        msg = "Fixing series description ({}) for {} scan {}".format(
            sd, xnat.session_label, s['ID'])

        # Only set if it's not already correct
        if sd != new_sd:
            pipe.log.info(msg)
            xnat.setScanElement(
                'xnat:mrScanData', 'series_description', new_sd)


def setScanTypes():
    with open(opts.scan_map) as f:
        scan_map = json.load(f)['scan_type_map']

    session_id = xnat.getSessionId()

    uri = '/data/experiments/{}/scans?columns=ID,xnat:mrScanData/parameters/imageType,series_description,type'.format(
        session_id)

    all_scans = xnat.getSessionScans()
    scans_with_imagetype = xnat.getJson(uri)
    scans_merged = []

    # The uri above doesn't give us non-mrScanData (Physio), so
    # we have to merge that information in with a listing of all scans
    for scan in all_scans:
        s = {}
        s['ID'] = scan['ID']
        s['type'] = scan['type']
        s['series_description'] = scan['series_description']

        for item in scans_with_imagetype:
            # There is a matching scan with image_type
            if item['ID'] == scan['ID']:
                s['image_type'] = item['xnat:mrscandata/parameters/imagetype']
                if s['image_type'].strip() == '':
                    s['image_type'] = 'NONE'

        # Couldn't find an image type for scan
        if 'image_type' not in s:
            s['image_type'] = 'NONE'

        scans_merged.append(s)

    for scan in scans_merged:
        xnat.scan_id = scan['ID']
        sd = scan['series_description']
        series_desc = ''.join(c for c in sd if c.isalnum()).upper()
        it = scan['image_type']
        image_type = ''.join(c for c in it if c.isalnum()).upper()
        if s['image_type'].strip() == '':
              s['image_type'] = 'NONE'
        scan_type = None

        # Look through scan mappings to find a match
        for mapping in scan_map:
            if mapping['series_description'] == series_desc and \
               mapping['image_type'] == image_type:

                scan_type = mapping['scan_type']
                break

        # Move on if we didn't find anything in mapping
        if not scan_type:
            msg = "{} ({} -- {}) not found in scan mapping {}".format(
                sd, series_desc, image_type, opts.scan_map)
            # print msg
            pipe.log.error(msg)
            continue

        # If we found a scan type in the mapping and it's not already set
        if scan['type'] != scan_type:
            msg = "Setting scan type for {} scan {}: {} ({}) --> {}".format(
                xnat.session_label, scan['ID'], sd, series_desc, scan_type)
            # print msg
            pipe.log.info(msg)
            xnat.setScanElement('xnat:mrScanData', 'type', scan_type)


if __name__ == "__main__":
    main()
